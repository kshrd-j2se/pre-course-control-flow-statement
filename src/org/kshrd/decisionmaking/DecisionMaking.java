package org.kshrd.decisionmaking;

public class DecisionMaking {

    // TODO: test with switch case
    static void switchCaseTest() {
        char grade = 'F';
        String result = "";


//        if (grade == 'A') {
//            result = "Excelent";
//        } else if (grade == 'B' || grade == 'C') {
//            result = "Well done";
//        } else if (grade == 'D') {
//            result = "Good";
//        } else if (grade == 'E') {
//            result = "You should try more";
//        } else if (grade == 'F') {
//            result = "Try again next year";
//        } else {
//            result = "Invalid grade";
//        }



        int score = 50;
        if (score < 50)

        switch (score) {
            case  50:
                result = "Excelent";
                break;
            case 'B':
            case 'C':
                result = "Well Done";
                break;
            case 'D':
                result = "Good";
                break;
            case 'E':
                result = "You should try more";
                break;
            case 'F':
                result = "Try again next year";
                break;
                default:
                    result = "Invalid grade";
        }

        System.out.println("Grade is : " + grade);
        System.out.println("Result is : " + result);

        /*
            a ---
            Grade is C
            Result is Well done

            b---
             Grade is C
            Result is Well done
            infinite run

            c ----
             Grade is C
            Result is Well Done
            good
            You should try more
            ...
            Invalid grade

            d ----
            Grade is C
            Invalid grade

         */

    }

    public static void main(String[] args) {
        DecisionMaking.switchCaseTest();
    }



}
